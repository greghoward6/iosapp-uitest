﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iOSTestCloud;
using Tests;

namespace App.Screens
{
    public static class AppScreens
    {
        public static HomeScreen HomeScreen
        {
            get { return new HomeScreen(); }
        }

        public static class ExploreScreens
        {
            public static JobTitleScreen JobTitleScreen
            {
                get { return new JobTitleScreen(); }
            }

            public static JobTypeScreen JobTypeScreen
            {
                get { return new JobTypeScreen(); }
            }

            public static LocationScreen LocationScreen
            {
                get { return new LocationScreen(); }
            }

            public static SalaryScreen SalaryScreen
            {
                get { return new SalaryScreen(); }
            }

            public static SectorsScreen SectorsScreen
            {
                get { return new SectorsScreen(); }
            }
        }

    }
}
