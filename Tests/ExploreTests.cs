﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Screens;
using NUnit.Framework;
using Xamarin.UITest;

namespace Tests
{
    [TestFixture]
    public class ExploreTests : BaseTest
    {
        [Test]
        public void Can_Explore()
        {
            // Load home screen
            AppScreens.HomeScreen.ExploreJobs();

            // Tap Explore button
            AppScreens.ExploreScreens.JobTitleScreen.EnterJobTitle();

            // Enter a job title

            // Select a sector

            // Enter a salary

            // Enter a location

            // Select job types
        }
    }
}
