﻿using System;
using Xamarin.UITest;

namespace Tests
{
    public class RegisterPage
    {
        private IApp _app;

        private const string PasswordEditTextMarked = "PasswordEditText";
        private const string EmailEditTextMarked = "EmailEditText";
        private const string FirstNameEditTextMarked = "FirstNameEditText";
        private const string LastNameEditTextMarked = "LastNameEditText";
        private const string SignInButtonMarked = "SignInRegBtn";

        public RegisterPage(IApp app)
        {
            _app = app;
            //WaitForPageLoad();
        }

        public void WaitForPageLoad()
        {
            //WAIT FOR SOMETHING UNIQUE ON THE PAGE
            //_app.WaitForElement("SOMETHING");
        }

        public void RegisterNewUser(string email, string pass, string first, string last)
        {
            _app.EnterText(EmailEditTextMarked, email);
            _app.DismissKeyboard();
            _app.Screenshot("Email address entered");

            _app.ScrollDownTo(PasswordEditTextMarked);

            // Password
            _app.EnterText(PasswordEditTextMarked, email);
            _app.DismissKeyboard();
            _app.Screenshot("Password entered");

            // First name
            _app.EnterText(FirstNameEditTextMarked, first);
            _app.DismissKeyboard();

            _app.ScrollDownTo(LastNameEditTextMarked);

            // Last name
            //_app.Tap(x => x.Marked("LastNameEditText"));
            _app.EnterText(LastNameEditTextMarked, last);
            _app.DismissKeyboard();

            _app.ScrollDownTo(SignInButtonMarked);

            // Tap Sign in button
            _app.Tap(SignInButtonMarked);
        }
    }
}

