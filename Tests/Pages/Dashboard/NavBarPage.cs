﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.UITest;
using System.Text;
using System.Threading.Tasks;

namespace Tests.Pages.DashboardNavBar
{
    class NavBarPage
    {
        private IApp _app;

        public NavBarPage(IApp app)
        {
            _app = app;
            WaitForPageLoad();
        }

        public void WaitForPageLoad()
        {
            //WAIT FOR SOMETHING UNIQUE ON THE PAGE
            //_app.WaitForElement("SOMETHING");
        }

        public void RefinementFilter()
        {
            _app.Tap(x => x.Marked"iconFilter"));
        }
        public void Feedback()
        {
            _app.Tap(x => x.Marked"iconFeedBck"));
        }

        public void Jobs()
        {
            _app.Tap(x => x.Marked"NavBar/dashboard-jobstack-unselected"));
        }
        public void MyJobs()
        {
            _app.Tap(x => x.Marked"NavBar/dashboard-heart-unselected"));
        }
        public void Settings()
        {
            _app.Tap(x => x.Marked"NavBar/dashboard-settings-unselected"));
        }
        public void Profile()
        {
            _app.Tap(x => x.Marked("NavBar/dashboard-profile-unselected"));
        }
  
    }
}