﻿using System;
using Xamarin.UITest;
using System.Linq;

namespace iOSTestCloud
{
	public class JobTypePage
	{
		private IApp _app;

		public JobTypePage(IApp app)
		{
			_app = app;
			WaitForPageLoad();
		}

		public void WaitForPageLoad()
		{
			//WAIT FOR SOMETHING UNIQUE ON THE PAGE
			//_app.WaitForElement("SOMETHING");
		}

		public void AddJobType()
		{
			_app.Tap(x => x.Marked("Permanent"));
			_app.Screenshot("Job type added");
			_app.Tap(x => x.Marked("Full-time"));
			_app.Screenshot("Job type added");
			_app.Tap(x => x.Marked("Part-time"));
			_app.Screenshot("Job type added");
			_app.Tap(x => x.Text("See job recommendations"));
		}
		public void ProceedToNextStep()
		{
			_app.Tap(x => x.Text("Next"));
		}
	}
}