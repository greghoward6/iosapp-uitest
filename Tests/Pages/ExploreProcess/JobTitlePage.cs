﻿using System;
using Xamarin.UITest;

namespace iOSTestCloud
{
    public class JobTitlePage
    {
        private IApp _app;

        public JobTitlePage(IApp app)
        {
            _app = app;
            WaitForPageLoad();
        }

        public void WaitForPageLoad()
        {
            //WAIT FOR SOMETHING UNIQUE ON THE PAGE
            //_app.WaitForElement("SOMETHING");
        }

        public void AddJobTitle(string title)
        {
            _app.EnterText("textview", title);
            _app.PressEnter();
        }

        public void ProceedToNextStep()
        {
            _app.Tap(x => x.Text("Next"));
        }
    }
}

