﻿using System;
using Xamarin.UITest;
using System.Linq;

namespace iOSTestCloud
{
	public class SalaryPage
	{
		private IApp _app;

		private const string SalaryEditTextMarked = "SalaryEditText";

		public SalaryPage(IApp app)
		{
			_app = app;
			WaitForPageLoad();
		}

		public void WaitForPageLoad()
		{
			//WAIT FOR SOMETHING UNIQUE ON THE PAGE
			//_app.WaitForElement("SOMETHING");
		}

		public void AddSalary(string salary)
		{
			_app.Tap(x => x.Marked("xxxxxxx"));
			_app.EnterText(SalaryEditTextMarked, salary);
			_app.Screenshot("Salary added");
			_app.PressEnter();
		}

		public void ProceedToNextStep()
		{
			_app.Tap(x => x.Text("Next"));
		}
	}
}