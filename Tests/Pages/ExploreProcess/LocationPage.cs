﻿using System;
using Xamarin.UITest;
using System.Linq;

namespace iOSTestCloud
{
		public class LocationPage
			{
			private IApp _app;

		private const string LocationEditTextMarked  = "XXXXXXX";

			public LocationPage(IApp app)
			{
				_app = app;
				WaitForPageLoad();
			}

			public void WaitForPageLoad()
			{
				//WAIT FOR SOMETHING UNIQUE ON THE PAGE
				//_app.WaitForElement("SOMETHING");
			}

			public void AddLocation(string location)
			{
			_app.Tap(x => x.Marked("XXXXXXX"));
			_app.PressEnter();
			_app.Tap(x => x.Text("Next"));
			}

			public void ProceedToNextStep()
			{
				_app.Tap(x => x.Text("Next"));
			}
		}
	}