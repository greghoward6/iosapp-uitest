﻿using System;
using Xamarin.UITest;
using System.Linq;

namespace iOSTestCloud
{
	public class SectorsPage
	{
		private IApp _app;

		public SectorsPage(IApp app)
		{
			_app = app;
			WaitForPageLoad();
		}

		public void WaitForPageLoad()
		{
			//WAIT FOR SOMETHING UNIQUE ON THE PAGE
			//_app.WaitForElement("SOMETHING");
		}

		public void AddSector()
		{
			while (true)
			{
				var selectSect = _app.Query(x => x.Text("Sales"));
				if (selectSect.Any())
				{
					_app.Tap(x => x.Text("Sales"));
					break;
				}
				_app.ScrollDown(); 
			}	
		}

		public void ProceedToNextStep()
		{
			_app.Tap(x => x.Text("Next"));
		}
	}
}