﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.UITest;

namespace Tests.Xamarin
{
    public static class Device
    {
        private static IApp App { get; set; }

        public static void Initialise()
        {
            App = ConfigureApp.iOS.DeviceIdentifier("e20426694d2756d0196db242d6998008bed1286e").InstalledApp("uk.co.reed.Jobs").StartApp();
        }

        public static IApp DeviceDriver
        {
            get { return App; }
        }

    }
}
