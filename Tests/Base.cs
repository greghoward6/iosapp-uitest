﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Tests.Xamarin;
using Xamarin.UITest;
using Xamarin.UITest.iOS;

namespace Tests
{
    public class Base
    {
        public void Tap(string accessibilityIdentifier)
        {
            Device.DeviceDriver.Tap(x => x.Marked(accessibilityIdentifier));
        }

        public void TypeInElement(string accessibilityIdentifier, string text)
        {
            Device.DeviceDriver.EnterText(accessibilityIdentifier, text);
        }
    }

}
