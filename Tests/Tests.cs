﻿using System;
using System.Linq;
using NUnit.Framework;
using Xamarin.UITest;
using Xamarin.UITest.iOS;
using Xamarin.UITest.Queries;
using Tests;
using System.Diagnostics;
using System.Threading;


namespace iOSTestCloud
{
    [TestFixture]
    public class Tests
    {
         iOSApp _app;

        [SetUp]
        public void BeforeEachTest()
        {
				{
				_app = ConfigureApp.iOS.DeviceIdentifier("e20426694d2756d0196db242d6998008bed1286e").
				InstalledApp("uk.co.reed.Jobs").
				StartApp();
				}
       		 }

        [Test]
        public void RegandEditProfile()
        {
            #region Sign in/Register view

            //Sign In button / Landing Screen

            _app.Tap("CreateAccountButton");
            _app.Screenshot("Register page should be displayed");

            // Email Address 
            var email = Helpers.GetRandomString(5) + "@autotestcloud.com";
            var pass = email;
            var first = Helpers.GetRandomString(7);
            var last = Helpers.GetRandomString(7);

            var registerPage = new RegisterPage(_app);
            registerPage.RegisterNewUser(email,pass,first,last);
            _app.Screenshot("Registered button tapped");

            #endregion

            #region Explore

            while(true)
            {
                if(_app.Query(x => x.Text("You're already registered")).Any())
                {
                    _app.Screenshot("Already registered");
                    _app.Tap(x => x.Text("OK"));
                    _app.EnterText("PasswordEditText",email);
                    _app.DismissKeyboard();
                    _app.Tap(x => x.Marked("SignInRegBtn"));
                    break;
                }

                if(_app.Query(e => e.Id("textview")).Any())
                {
                    //// Explore
                    // Job title
                    var jobTitlePage = new JobTitlePage(_app);
                    jobTitlePage.AddJobTitle("Sales assistant");
                    jobTitlePage.AddJobTitle("Sales executive");

                    _app.Screenshot("Job title added");
                    jobTitlePage.ProceedToNextStep();

                    // Sectors

					var sectorsPage = new SectorsPage(_app);
					sectorsPage.AddSector();
					_app.Screenshot("Sectors added");
					sectorsPage.ProceedToNextStep();
					       

                    // Salary
					var salaryPage = new SalaryPage(_app);
					salaryPage.AddSalary("18000");
					_app.Screenshot("Salary added");
					salaryPage.ProceedToNextStep();

                    // Locations
					var locationPage = new LocationPage(_app);
					locationPage.AddLocation("London");
					_app.Screenshot("Location added");
					locationPage.ProceedToNextStep();
                                   

                    // Job Type
					var jobtypePage = new JobTypePage(_app);
					jobtypePage.AddJobType();
					_app.Screenshot("Job type added");
					jobTitlePage.ProceedToNextStep();
					                  
                }
                    
            }

            #endregion

            #region Tabs

            _app.WaitForNoElement("logo");
            _app.Tap("tutorialOverlay");

            // Navigate to profile 
            _app.Tap((x => x.Marked("profiletab")));
            while (true)
            {
                var Profile = _app.Query(x => x.Text("About Me"));

                if (Profile.Any())
                {
                    Assert.IsTrue(Profile.Any(), "Profile loaded");
                    break;
                }
                else
                {
                    _app.ResiliantTapWithDelay(x => x.Text("Build manually"));
                    break;
                }
            }

            _app.Screenshot("Profile tab");

            #endregion

            #region Mandatory fields 

            while (true)
            {
                _app.ScrollDown();
                var countryQuery = _app.Query(x => x.Text("Choose"));
                if (countryQuery.Any())
                {
                    _app.ResiliantTapWithDelay(x => x.Text("Choose"));
                    _app.ResiliantTapWithDelay(x => x.Text("United Kingdom"));
                    break;
                }
            }

            _app.ResiliantTapWithDelay(x => x.Marked("PostcodeEditText"));
            _app.EnterText("WC2B5LX");
            _app.DismissKeyboard();
            _app.ResiliantTapWithDelay(x => x.Marked("PhoneNumberEditText"));
            _app.EnterText(Helpers.GetRandomNumber(11));
            _app.DismissKeyboard();
            _app.ResiliantTapWithDelay(x => x.Marked("AboutMeEligibileToWorkInUKButton"));
            _app.WaitForElement(x => x.Text("Eligibility"));
            _app.ResiliantTapWithDelay(x => x.Text("Choose"));
            _app.ResiliantTapWithDelay(x => x.Text("British"));
            _app.ResiliantTapWithDelay(x => x.Marked("actionbar_BackButton"));
            _app.Screenshot("Mandatory fields added");


            #endregion

            #region Covering letter

            // while (true)
            // {
            //     _app.ScrollDown();
            //     var coverLet = _app.Query(x => x.Text("Add cover letter"));
            //     if (coverLet.Any())
            //     {
            //         _app.Tap(x => x.Marked("EditCoverLetter"));
            //         break;
            //         }

            // }


            //// _app.WaitForElement(x => x.Text("Cover letter"));
            // _app.EnterText("Testing covering letter saves once text has been entered");
            // _app.Tap(x => x.Text("Save"));
            // Thread.Sleep(200);

            // while (true)
            // {
            //     _app.ScrollDown();
            //     var coverLet = _app.Query(x => x.Text("Testing covering letter saves once text has been entered"));
            //     if (coverLet.Any())
            //     {
            //         var coveringLetter =
            //             _app.Query(x => x.Text("Testing covering letter saves once text has been entered"));
            //         Assert.IsTrue(coveringLetter.Any(), "covering letter contains new values");
            //         break;
            //     }
            // }



            // _app.Screenshot("covering letter");

            #endregion

            #region work history

            var sucess = false;
            var timeout = TimeSpan.FromSeconds(15);
            var stopwatch = Stopwatch.StartNew();

            while (!sucess)
            {
                if(stopwatch.Elapsed >= timeout)
                {
                    throw new TimeoutException("Unable to find work history / share screen");
                }

                //THIS IS PROBABLY A BUG....
                try
                {
                    _app.ScrollUp();
                }
                catch(Exception)
                {
                    var share = _app.Query("dropboxbutton");
                    if(share.Any())
                    {
                        _app.Back();
                    }
                }
                finally
                {
                    var countryQuery = _app.Query(x => x.Text("Add work history"));
                    if (countryQuery.Any())
                    {
                        _app.Tap(x => x.Text("Add work history"));
                        sucess = true;
                        stopwatch.Stop();
                    } 
                }

            }

            _app.ResiliantTapWithDelay(x => x.Marked("jobtitleentry"));
            _app.EnterText("My job");
            _app.DismissKeyboard();
            _app.ResiliantTapWithDelay(x => x.Marked("companynameentry"));
            _app.EnterText("My company 1");
            _app.DismissKeyboard();
            _app.ResiliantTapWithDelay(x => x.Marked("fromdatebtn"));

            _app.ResiliantTapWithDelay(x => x.Marked("button1"));
            _app.ResiliantTapWithDelay(x => x.Marked("ChckBxICurrentlyWorkHere"));

            _app.ResiliantTapWithDelay(x => x.Marked("actionbar_NextButton"));
            _app.Screenshot("first work history added");
            _app.WaitFor(() => _app.Query(x => x.Text("My company 1")).Any());
            var wrkHis = _app.Query(x => x.Text("My company 1"));
            Assert.IsTrue(wrkHis.Any(), "work history visible on profile");

            while (true)
            {
                var addWorkHis = _app.Query(x => x.Text("Add work history"));
                if (addWorkHis.Any())
                {
                    _app.Tap(x => x.Text("Add work history"));
                    break;
                }
                _app.ScrollDown("content_frame", ScrollStrategy.Gesture);
            }

            _app.ResiliantTapWithDelay(x => x.Marked("jobtitleentry"));
            _app.EnterText("My job 2");
            _app.DismissKeyboard();
            _app.ResiliantTapWithDelay(x => x.Marked("companynameentry"));
            _app.EnterText("My company 2");
            _app.DismissKeyboard();
            _app.ResiliantTapWithDelay(x => x.Marked("fromdatebtn"));
            _app.ResiliantTapWithDelay(x => x.Marked("button1"));
            _app.ResiliantTapWithDelay(x => x.Marked("ChckBxICurrentlyWorkHere"));
            _app.ResiliantTapWithDelay(x => x.Marked("actionbar_NextButton"));
            Thread.Sleep(800);
            _app.Screenshot("work history added");
            _app.WaitFor(() => _app.Query(x => x.Text("My company 2")).Any());
            var wrkHis2 = _app.Query(x => x.Text("My company 2"));
            Assert.IsTrue(wrkHis2.Any(), "work history visible on profile");

            #endregion

            #region qualification

            #region qualification 1

            _app.ScrollUpTo("name","content_frame");


            //THIS IS PROBABLY A BUG....
            try
            {
                _app.ScrollDownTo("Add qualification","content_frame");
            }
            catch(Exception)
            {
                if(_app.Query("United Kingdom").Any())
                    _app.Back();
            }
            finally
            {
                var qualifactionQuery = _app.Query(x => x.Text("Add qualification"));
                if (qualifactionQuery.Any())
                {
                    _app.Tap(x => x.Text("Add qualification"));
                }
            }

            _app.ResiliantTapWithDelay(x => x.Marked("QualificationTypeSpinner"));
            _app.ResiliantTapWithDelay(x => x.Text("GCSE"));
            _app.EnterText("institution","My school 1");
            _app.DismissKeyboard();
            _app.EnterText("subject","My subject 1");
            _app.DismissKeyboard();
            _app.ResiliantTapWithDelay(x => x.Marked("fromdatebtn"));
            _app.ResiliantTapWithDelay(x => x.Marked("button1"));
            _app.ResiliantTapWithDelay(x => x.Marked("ChckBxICurrentlyStudyHere"));
            _app.ScrollDownTo(x => x.Text("Please select"));
            _app.ResiliantTapWithDelay(x => x.Text("Please select"));
            _app.ResiliantTapWithDelay(x => x.Text("B"));
            _app.ResiliantTapWithDelay(x => x.Marked("actionbar_NextButton"));

            #endregion

            #region qualification 2

            _app.ScrollUpTo("name","content_frame");

            //THIS IS PROBABLY A BUG....
            try
            {
                _app.ScrollDownTo("Add qualification","content_frame");
            }
            catch(Exception)
            {
                if(_app.Query("United Kingdom").Any())
                    _app.Back();
            }
            finally
            {
                var qualifactionQuery = _app.Query(x => x.Text("Add qualification"));
                if (qualifactionQuery.Any())
                {
                    _app.Tap(x => x.Text("Add qualification"));
                }
            }

//            _app.ScrollUpTo("name","content_frame");
//            _app.ScrollDownTo("AddEducation","content_frame");
//
//            var educationQuery = _app.Query(x => x.Marked("AddEducation"));
//            if (educationQuery.Any())
//            {
//                _app.Tap(x => x.Marked("AddEducation"));
//            }
//
//            Thread.Sleep(300);

            _app.ResiliantTapWithDelay(x => x.Marked("QualificationTypeSpinner"));
            _app.ResiliantTapWithDelay(x => x.Text("A-level"));
            _app.ResiliantTapWithDelay(x => x.Marked("institution"));
            _app.EnterText("My school 2");
            _app.DismissKeyboard();
            _app.EnterText("subject","My subject 2");
            _app.DismissKeyboard();
            _app.ResiliantTapWithDelay(x => x.Marked("fromdatebtn"));
            _app.ResiliantTapWithDelay(x => x.Marked("button1"));
            _app.ResiliantTapWithDelay(x => x.Marked("ChckBxICurrentlyStudyHere"));
            _app.ScrollDownTo(x => x.Text("Please select"));
            _app.ResiliantTapWithDelay(x => x.Text("Please select"));
            _app.ResiliantTapWithDelay(x => x.Text("A"));
            _app.ResiliantTapWithDelay(x => x.Marked("actionbar_NextButton"));
            _app.Screenshot("Qualification 2 added");
            _app.WaitFor(() => _app.Query(x => x.Text("My school 2")).Any());
            var qual2 = _app.Query(x => x.Text("My school 2"));
            Assert.IsTrue(qual2.Any(), "verifying that qualification 2 is visible");


            #endregion

            #region qualification 3

            //Thread.Sleep(300);

            while (true)
            {
                var allQqualifactionQuery = _app.Query(x => x.Text("View all qualifications"));
                if (allQqualifactionQuery.Any())
                {
                    _app.Query(x => x.Text("View all qualifications"));
                    break;
                }
                _app.ScrollDown("content_frame", ScrollStrategy.Gesture);
            }

            _app.ResiliantTapWithDelay(x => x.Text("View all qualifications"));
            _app.ResiliantTapWithDelay(x => x.Text("Add"));
            _app.ResiliantTapWithDelay(x => x.Marked("QualificationTypeSpinner"));
            _app.ResiliantTapWithDelay(x => x.Text("University Degree"));
            _app.ResiliantTapWithDelay(x => x.Marked("institution"));
            _app.EnterText("My school 3");
            _app.DismissKeyboard();
            _app.ResiliantTapWithDelay(x => x.Marked("coursetitle"));
            _app.EnterText("My subject 3");
            _app.Back();
            _app.ResiliantTapWithDelay(x => x.Marked("fromdatebtn"));
            _app.ResiliantTapWithDelay(x => x.Marked("button1"));
            _app.ResiliantTapWithDelay(x => x.Marked("ChckBxICurrentlyStudyHere"));
            _app.ScrollDownTo(x => x.Text("Please select"));
            _app.ResiliantTapWithDelay(x => x.Text("Please select"));
            _app.ResiliantTapWithDelay(x => x.Text("2:2"));
            _app.ResiliantTapWithDelay(x => x.Marked("actionbar_NextButton"));
            _app.Screenshot("Qualification 3 added");
            _app.ResiliantTapWithDelay(x => x.Id("actionbar_BackButton"));
            //_app.Back();
            _app.Screenshot("Qualifications added to profile");

            while (true)
            {
                var allQqualifactionQuery = _app.Query(x => x.Text("View all qualifications"));
                if (allQqualifactionQuery.Any())
                {
                    _app.Query(x => x.Text("View all qualifications"));
                    break;
                }
                _app.ScrollDown("content_frame", ScrollStrategy.Gesture);
            }

            _app.WaitFor(() => _app.Query(x => x.Text("View all qualifications")).Any());
            var qual4 = _app.Query(x => x.Text("View all qualifications"));
            Assert.IsTrue(qual4.Any(), "view all button visible on profile");

            #endregion


            #endregion

            #region personal statement

            while (true)
            {
                _app.ScrollUpTo("AboutMeSummaryHeader","content_frame", ScrollStrategy.Gesture);
                var perState = _app.Query(x => x.Marked("AboutMeSummaryHeader"));
                if (perState.Any())
                {
                    _app.Tap(x => x.Marked("EditSummary"));
                    break;
                }
            }

            _app.ResiliantTapWithDelay(x => x.Marked("texttoedit"));
            //Thread.Sleep(500);
            _app.EnterText("This is my personal statement");
            _app.ResiliantTapWithDelay(x => x.Text("Save"));
            _app.Screenshot("Personal statement updated");
            Thread.Sleep(500);
            var newPerState = _app.Query(x => x.Text("This is my personal statement"));
            Assert.IsTrue(newPerState.Any(), "Personal statement updated");
            Thread.Sleep(500);


            #endregion
        }
    }
}

