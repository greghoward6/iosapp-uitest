﻿using System;
using System.Threading.Tasks;

namespace Tests
{
    public static class Helpers
    {
        public static string GetRandomString(int size)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
            var random = new Random();
            var buffer = new char[size];

            for (int i = 0; i < size; i++)
            {
                buffer[i] = chars[random.Next(chars.Length)];
            }
            return new string(buffer);
        }

        public static string GetRandomNumber(int size)
        {
            const string numbers = "0123456789";
            var random = new Random();
            var buffer = new char[size];

            for (int i = 0; i < size; i++)
            {
                buffer[i] = numbers[random.Next(numbers.Length)];
            }
            return new string(buffer);

        }

        public static async void LogAsync(string message)
        {
            await Task.Factory.StartNew(() => 
                Console.WriteLine(String.Format("*** DEBUG message: {0} <-> timestamp: {1}" , 
                    message, 
                    DateTime.UtcNow.ToString()
                ))
            );
        }
    }
}

