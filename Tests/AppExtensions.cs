﻿using System;
using Xamarin.UITest;
using Xamarin.UITest.Queries;
using System.Diagnostics;
using System.Threading;
using System.Linq;

namespace Tests
{
    public static class AppExtensions
    {
        public static void ResiliantTapWithDelay(this IApp app, Func<AppQuery,AppQuery> func, int delay = 0, int interval = 100, double timeout = 15)
        {
            var stopwatch = Stopwatch.StartNew();
            var timespanTimeout = TimeSpan.FromSeconds(timeout);

            while (true)
            {
                if (stopwatch.Elapsed >= timespanTimeout)
                    throw new TimeoutException("Timed out waiting to resiliant tap element");

                Thread.Sleep(delay);

                var appResult = app.Query(func);

                if (appResult.Any())
                {
                    Helpers.LogAsync("Resiliant Tap Button");
                    app.Tap(func);
                    break;
                }

                Thread.Sleep(interval);
            }
        }
    }
}

